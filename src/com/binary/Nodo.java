package com.binary;
/**
 * Clase que tiene el comportamiento de un nodo
 * <br>por defecto es una hoja
 * @author oscar
 *
 */
public class Nodo extends Hoja {
	Rama rama;
	/**
	 * Constructor que permite asiganar una rama a un nodo,
	 * <br>por defecto el comportamiento es un nodo tipo hoja.
	 * @param valor
	 */
	public Nodo(int valor) {
		super(valor);
	}

	public Rama getRama() {
		return rama;
	}

	public void setRama(Rama rama) {
		this.rama = rama;
	}

}
