package com.binary;

import com.binary.road.PathBinaryTree;

public class Test {

	public static void main(String[] args) {
		
		Nodo tree = new Nodo(104);
		Nodo nodoA1 = new Nodo(71);
		Nodo nodoA2 = new Nodo(17);
		nodoA2.setRama(new Rama(new Nodo(3), new Nodo(18)));
		nodoA1.setRama(new Rama(nodoA2,new Nodo(19)));		
		Nodo NodoB1=new Nodo(240);
		Nodo nodoB2=new Nodo(108);
		nodoB2.setRama(new Rama(null, new Nodo(110)));
		NodoB1.setRama(new Rama(nodoB2,new Nodo(245)));
		Rama rama = new Rama(nodoA1,NodoB1);		
		tree.setRama(rama);
		System.out.println(PathBinaryTree.preOrden(tree));
		System.out.println(PathBinaryTree.inOrden(tree));
		System.out.println(PathBinaryTree.postOrden(tree));
	}
	
	
	
	
}
