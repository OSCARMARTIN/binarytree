package com.binary.road;

import com.binary.Nodo;
public final class PathBinaryTree {
	
	/**
	 * 
	 * @param tree
	 * @return
	 */
	public static String preOrden(Nodo tree) {
		StringBuilder r = new StringBuilder();
		if(tree!=null) {
			r.append(tree.getValor()+",");
			if(tree.getRama()!=null) {
				r.append(preOrden(tree.getRama().getNodoA()));
				r.append(preOrden(tree.getRama().getNodoB()));
			}
		}
		return r.toString();
		
	}
	
	/**
	 * 
	 * @param tree
	 * @return
	 */
	public static String inOrden(Nodo tree) {
		StringBuilder r = new StringBuilder();
		if(tree!=null) {
			
			if(tree.getRama()!=null) {
				r.append(inOrden(tree.getRama().getNodoA()));
			}
				r.append(tree.getValor()+",");
			if(tree.getRama()!=null) {
				r.append(inOrden(tree.getRama().getNodoB()));
			}
		}
		return r.toString();
		
	}
	
	/**
	 * 
	 * @param tree
	 * @return
	 */
	public static String postOrden(Nodo tree) {
		StringBuilder r = new StringBuilder();
		if(tree!=null) {			
			if(tree.getRama()!=null) {
				r.append(postOrden(tree.getRama().getNodoA()));
				r.append(postOrden(tree.getRama().getNodoB()));				
			}
			r.append(tree.getValor()+",");
		}
		return r.toString();
		
	}
}
