package com.binary;
/**
 * Clase con el valor fundamental del nodo
 * @author oscar
 *
 */
public class Hoja {
	int valor;
	
	public Hoja(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}
