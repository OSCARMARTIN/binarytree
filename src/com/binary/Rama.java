package com.binary;

/**
 * Clase que permite crear un subArbol
 * @author oscar
 *
 */
public class Rama {
	Nodo nodoA;
	Nodo nodoB;
	/**
	 * Constructor que permite asiganar los nodos A(izquierdo) y B(Derecho)
	 * @param nodoA
	 * @param nodoB
	 */
	public Rama(Nodo nodoA, Nodo nodoB) {
		super();
		this.nodoA = nodoA;
		this.nodoB = nodoB;
	}
	
	public Nodo getNodoA() {
		return nodoA;
	}
	public void setNodoA(Nodo nodoA) {
		this.nodoA = nodoA;
	}
	public Nodo getNodoB() {
		return nodoB;
	}
	public void setNodoB(Nodo nodoB) {
		this.nodoB = nodoB;
	}
}