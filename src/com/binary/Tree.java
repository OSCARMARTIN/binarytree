package com.binary;
/**
 * 
 * @author oscar
 * Clase que permite iniciar un arbol
 */
public class Tree {
	int raiz;
	Rama rama;
	/**
	 * Obtiene Valor raiz de cada subarbol
	 * @return
	 */
	public int getRaiz() {
		return raiz;
	}
	/**
	 * Asigna el valor de cada raiz
	 * @param raiz
	 */
	public void setRaiz(int raiz) {
		this.raiz = raiz;
	}
	public Rama getRama() {
		return rama;
	}
	public void setRama(Rama rama) {
		this.rama = rama;
	}
}
